const express = require("express");
const path = require('path');
const app = express();
const {User_game} = require("./models");

app.set("view engine", "ejs");
app.use(express.urlencoded({
  extended: false,
}));

app.use('/assets',express.static(path.join(__dirname, 'assets')));

app.get("/", (req, res) => {
  res.render("auth/login");
});

app.post("/login", (req, res) => {
  const {username, password} = req.body;
  if(username == "ivan" && password == "avx"){
    res.redirect("/home");
  }
  else{
    res.redirect("/");
  }
})

app.get("/home", (req, res) => {
  User_game.findAll().then((user_game) => {
    res.render("user_game", {
      user_game,
    });
  });
});

app.get("/user_game/create", (req, res) => {
  res.render("user_game/create");
});

app.post("/user_game", (req, res) => {
  User_game.create({
    username: req.body.username,
    password: req.body.password,
  }).then((user_game) => {
    res.render("user_game/created");
  });
});

app.get('/user_game/edit/(:id)', (req, res) => {
  User_game.findOne({
    where: {id: req.params.id},
  }).then((user_game) => {
    res.render("user_game/edit", {
      id: user_game.id,
      username: user_game.username,
      password: user_game.password
    });
  });
});

app.post("/user_game/update/(:id)", (req, res) => {
  User_game.update({
    username: req.body.username,
    password: req.body.password,
  },
  {
    where: {id: req.params.id},
  }).then((user_game) => {
    res.render("user_game/updated");
  }).catch((err) => {
    res.status(422).json("Can't update an user_game")
  });
})

app.get('/user_game/delete/(:id)', (req, res) => {
    User_game.destroy({
      where: {id: req.params.id},
    }).then((user_game) => {
      res.render("user_game/deleted");
    });
});

const port = 3000
app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});